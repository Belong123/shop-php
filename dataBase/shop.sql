/*
Navicat MySQL Data Transfer

Source Server         : PHP
Source Server Version : 50731
Source Host           : localhost:3306
Source Database       : shop

Target Server Type    : MYSQL
Target Server Version : 50731
File Encoding         : 65001

Date: 2021-07-12 11:53:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_commo
-- ----------------------------
DROP TABLE IF EXISTS `tb_commo`;
CREATE TABLE `tb_commo` (
  `id` int(4) NOT NULL AUTO_INCREMENT COMMENT '自动编号',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '商品名称',
  `pics` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pics/null.jpg' COMMENT '商品图片',
  `info` mediumtext COLLATE utf8_unicode_ci NOT NULL COMMENT '商品介绍',
  `addtime` date NOT NULL COMMENT '添加时间',
  `area` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '商品产地',
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '商品型号',
  `class` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '商品类型',
  `brand` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '品牌',
  `stocks` int(4) NOT NULL DEFAULT '1' COMMENT '商品库存',
  `sell` int(4) NOT NULL DEFAULT '0' COMMENT '销售量',
  `m_price` float NOT NULL COMMENT '市场价格',
  `v_price` float NOT NULL COMMENT '会员价格',
  `fold` float NOT NULL DEFAULT '9' COMMENT '打折率',
  `isnew` int(1) NOT NULL DEFAULT '1' COMMENT '是否新品',
  `isnom` int(1) NOT NULL DEFAULT '0' COMMENT '是否推荐',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_commo
-- ----------------------------
INSERT INTO `tb_commo` VALUES ('24', '你的名字手办 七夕情人节纪念礼物立花瀧宫水三情侣摆件饰品', 'pics/你的名字手办 七夕情人节纪念礼物立花瀧宫水三情侣摆件饰品.jpg', '你的名字手办 七夕情人节纪念礼物立花瀧宫水三情侣摆件饰品.jpg', '2021-06-03', '中国', '0001-05', '手办', '动漫周边', '10', '1', '388', '349.2', '9', '1', '0');
INSERT INTO `tb_commo` VALUES ('23', '寿屋 天使的心跳 Beats 脉动', 'pics/寿屋 天使的心跳 Beats 脉动 .jpg', '寿屋 天使的心跳 Beats 脉动 .jpg', '2021-06-03', '中国', '003-0122', '手办', 'b站', '3', '3', '88', '79.2', '9', '1', '1');
INSERT INTO `tb_commo` VALUES ('21', 'overlord 雅儿贝德手办骨王不死者', 'pics/overlord 雅儿贝德手办骨王不死者.jpg', 'overlord 雅儿贝德手办骨王不死者.jpg', '2021-06-03', '中国', '002-0001', '手办', '动漫周边', '1', '1', '88', '79.2', '9', '1', '1');
INSERT INTO `tb_commo` VALUES ('20', 'Fate短袖T恤幸运石二次元动漫周边', 'pics/Fate短袖T恤幸运石二次元动漫周边.jpg', 'Fate短袖T恤幸运石二次元动漫周边', '2009-05-03', '中国', '0001-01', '衣服', 'Fate', '112', '9', '88', '79.2', '9', '1', '1');
INSERT INTO `tb_commo` VALUES ('26', '六一儿童节礼物男孩女孩一禅小和尚生日礼物', 'pics/六一儿童节礼物男孩女孩一禅小和尚生日礼物.jpg', '六一儿童节礼物男孩女孩一禅小和尚生日礼物.jpg', '2021-06-03', '中国', '0012211', '玩具', '动漫周边', '1', '0', '69', '62.1', '9', '1', '1');
INSERT INTO `tb_commo` VALUES ('27', '穿书自救指南手表幸运石动漫周边师尊沈清秋洛冰河石英表', 'pics/穿书自救指南手表幸运石动漫周边师尊沈清秋洛冰河石英表.jpg', '穿书自救指南手表幸运石动漫周边师尊沈清秋洛冰河石英表', '2021-06-03', '中国', 'ALAK089', '手表', 'b站', '99', '4', '199', '159.2', '8', '1', '1');
INSERT INTO `tb_commo` VALUES ('28', '擎苍狐妖小红娘涂山雅雅Q版手办动漫周边', 'pics/擎苍狐妖小红娘涂山雅雅Q版手办动漫周边.jpg', '擎苍狐妖小红娘涂山雅雅Q版手办动漫周边.jpg', '2018-08-09', '中国', 'EOS M50(15-45)', '动漫周边', 'b站', '1', '0', '299', '269.1', '9', '0', '1');
INSERT INTO `tb_commo` VALUES ('29', '超多维SuperD裸眼悬浮3d BOX游戏影院智能高科技一体机vr虚拟现实', 'pics/超多维SuperD裸眼悬浮3d BOX游戏影院智能高科技一体机vr虚拟现实.jpg', '超多维SuperD裸眼悬浮3d BOX游戏影院智能高科技一体机vr虚拟现实', '2018-08-17', '中国', '0000', 'VR', '超多维', '20', '1', '199999', '159999', '8', '1', '0');
INSERT INTO `tb_commo` VALUES ('30', '初音未来手办miku和服雪樱花蝴蝶公主周边模型摆件公仔玩偶可动 054雪初音可动15cm 国产版', 'pics/初音未来手办miku和服雪樱花蝴蝶公主周边模型摆件公仔玩偶可动 054雪初音可动15cm 国产版.jpg', '初音未来手办miku和服雪樱花蝴蝶公主周边模型摆件公仔玩偶可动 054雪初音可动15cm 国产版', '2021-06-04', '中国', '0000', '手办', '人物', '999', '145', '124', '111.6', '9', '0', '0');
INSERT INTO `tb_commo` VALUES ('31', '手办动漫二次元可爱少女模型汽车装饰缘之空穹妹和服春日野穹摆件 白色', 'pics/手办动漫二次元可爱少女模型汽车装饰缘之空穹妹和服春日野穹摆件 白色.jpg', '手办动漫二次元可爱少女模型汽车装饰缘之空穹妹和服春日野穹摆件 白色.jpg', '2018-08-17', '中国', '0000', '手办', 'b站', '10', '1', '360', '266', '9', '1', '0');
INSERT INTO `tb_commo` VALUES ('32', '正版授权 bilibili哔哩哔哩动漫周边', 'pics/正版授权 bilibili哔哩哔哩动漫周边.jpg', '正版授权 bilibili哔哩哔哩动漫周边.jpg', '2021-06-03', '中国', '0000', '动漫', 'b站', '666', '17', '199', '159.2', '10', '1', '0');
INSERT INTO `tb_commo` VALUES ('33', '雪初音未来耳机MIKU无线蓝牙概念运动骑行眼镜', 'pics/雪初音未来耳机MIKU无线蓝牙概念运动骑行眼镜.jpg', '雪初音未来耳机MIKU无线蓝牙概念运动骑行眼镜.jpg', '2021-06-03', '中国', '0000', '耳机', 'Herman Miller SAYL', '66', '4', '499', '449.1', '9', '1', '1');
INSERT INTO `tb_commo` VALUES ('34', '至唯休闲鞋鬼灭之刃鞋子运动鞋炭治郎动漫周边高帮板鞋潮男联名鞋子', 'pics/至唯休闲鞋鬼灭之刃鞋子运动鞋炭治郎动漫周边高帮板鞋潮男联名鞋子 .jpg', '至唯休闲鞋鬼灭之刃鞋子运动鞋炭治郎动漫周边高帮板鞋潮男联名鞋子 .jpg', '2021-06-03', '中国', 'CT1609', '鞋子', '动漫', '999', '46', '188', '155', '9', '1', '1');
INSERT INTO `tb_commo` VALUES ('35', '初音未来手办miku和服雪樱花蝴蝶公主周边模型摆件公仔玩偶可动 蝴蝶赛车初音23cm 国产版', 'pics/初音未来手办miku和服雪樱花蝴蝶公主周边模型摆件公仔玩偶可动 蝴蝶赛车初音23cm 国产版.jpg', '初音未来手办miku和服雪樱花蝴蝶公主周边模型摆件公仔玩偶可动 蝴蝶赛车初音23cm 国产版', '2021-06-01', '中国', '00000', '手办', '人物', '999', '56', '299', '239.2', '8', '1', '1');
INSERT INTO `tb_commo` VALUES ('36', '正版授权 bilbili哔哩哔哩动漫周边 动画版天官赐福周边', 'pics/正版授权 bilbili哔哩哔哩动漫周边 动画版天官赐福周边.jpg', '正版授权 bilbili哔哩哔哩动漫周边 动画版天官赐福周边', '2021-06-03', '中国', 'E74003A', '动画', '动画', '56', '4', '349', '279.2', '8', '1', '1');
INSERT INTO `tb_commo` VALUES ('37', '漫易二次元明日游戏方舟周边阿米娅年.jpg', 'pics/漫易二次元明日游戏方舟周边阿米娅年.jpg', '漫易二次元明日游戏方舟周边阿米娅年', '2021-06-03', '中国', '5347329', '游戏', '游戏', '24', '3', '99', '89.1', '9', '1', '1');
INSERT INTO `tb_commo` VALUES ('38', '擎苍全职高手苏沐橙Q版手办动漫周边二次元摆件玩偶公仔娃娃少女儿童可爱古典国风', 'pics/擎苍全职高手苏沐橙Q版手办动漫周边二次元摆件玩偶公仔娃娃少女儿童可爱古典国风.jpg', '擎苍全职高手苏沐橙Q版手办动漫周边二次元摆件玩偶公仔娃娃少女儿童可爱古典国风', '2018-08-21', '中国', '111111', '手办', '手办', '400', '124', '149', '134.1', '9', '1', '1');
INSERT INTO `tb_commo` VALUES ('39', '萌创社 公主连结凯露臭鼬娘动漫二次元周边接头霸王趴趴枕抱枕礼物 臭鼬头1 40x50自然绒趴趴枕', 'pics/萌创社 公主连结凯露臭鼬娘动漫二次元周边接头霸王趴趴枕抱枕礼物 臭鼬头1 40x50自然绒趴趴枕.jpg', '萌创社 公主连结凯露臭鼬娘动漫二次元周边接头霸王趴趴枕抱枕礼物 臭鼬头1 40x50自然绒趴趴枕', '2021-05-27', '中国', 'M79250BM-0001', '动漫周边', '动漫周边', '99', '4', '55', '49.5', '9', '1', '1');
INSERT INTO `tb_commo` VALUES ('40', '萌创社 蕾姆双层军牌动漫项链异世界生活周边二次元钛钢吊坠牌 蕾姆 双层军牌项链', 'pics/创社 蕾姆双层军牌动漫项链异世界生活周边二次元钛钢吊坠牌 蕾姆 双层军牌项链.jpg', '蕾姆双层军牌动漫项链', '2021-06-01', '中国', 'sk-iisk2', '动漫周边', 'skin2', '155', '9', '88', '79.2', '9', '0', '1');
INSERT INTO `tb_commo` VALUES ('41', '我的青春恋爱物语果然有问题 雪之下雪乃', 'pics/我的青春恋爱物语果然有问题 雪之下雪乃.jpg', '我的青春恋爱物语果然有问题 雪之下雪乃.jpg', '2021-06-03', '中国', '304/306', '手办', '人物', '888', '179', '299', '269.1', '9', '1', '0');
INSERT INTO `tb_commo` VALUES ('42', '手游崩坏学园3 八重樱 真炎幸魂 盒装手办 送男友女生生日礼物', 'pics/手游崩坏学园3 八重樱 真炎幸魂 盒装手办 送男友女生生日礼物.jpg', '手游崩坏学园3 八重樱 真炎幸魂 盒装手办 送男友女生生日礼物.jpg', '2018-08-22', '中国', 'RH2021 ', '手办', '动漫周边', '1', '0', '88', '79', '9', '1', '0');
INSERT INTO `tb_commo` VALUES ('43', '正版哔哩哔哩bilibili周边2020新版小电视泡面碗套装二次元 小电视泡面碗 现货', 'pics/正版哔哩哔哩bilibili周边2020新版小电视泡面碗套装二次元 小电视泡面碗 现货.jpg', 'bilibili哔哩哔哩2233娘双面樱花折扇 印花扇子 B站周边 2233娘 双面樱花折扇', '2021-06-03', '中国', 'BCD-572WDENU1', '杯子', '杯子', '47', '56', '200', '180', '9', '1', '1');
INSERT INTO `tb_commo` VALUES ('44', '漫宅坊五等分的花嫁darling.jpg', 'pics/漫宅坊五等分的花嫁darling.jpg', '漫宅坊五等分的花嫁darling.jpg', '2018-08-02', '中国', 'Envy/X360/15-bp101TX', '耳机', '耳机', '266', '13', '199', '189', '9.5', '1', '0');
INSERT INTO `tb_commo` VALUES ('45', '擎苍魔道祖师魏无羡蓝忘机Q版手办玩具动漫卡通周边二次元.', 'pics/擎苍魔道祖师魏无羡蓝忘机Q版手办玩具动漫卡通周边二次元.jpg', '擎苍魔道祖师魏无羡蓝忘机Q版手办玩具动漫卡通周边二次元.jpg', '2021-06-03', '中国', '11111', '动漫周边', '动漫周边', '1', '0', '188', '245', '9', '1', '0');
INSERT INTO `tb_commo` VALUES ('46', '漫宅坊动漫约会大作战文豪野犬地缚少年花子君明日狂三方舟周边', 'pics/漫宅坊动漫约会大作战文豪野犬地缚少年花子君明日狂三方舟周边.jpg', '漫宅坊动漫约会大作战文豪野犬地缚少年花子君明日狂三方舟周边', '2021-06-03', '中国', 'mini', '动漫周边', '无', '1', '0', '88', '133', '9', '1', '0');
INSERT INTO `tb_commo` VALUES ('47', '怒风滑稽斜眼笑脸恶搞表情包衣服男女动漫周边学生短袖T恤夏可订制', 'pics/怒风滑稽斜眼笑脸恶搞表情包衣服男女动漫周边学生短袖T恤夏可订制.jpg', '怒风滑稽斜眼笑脸恶搞表情包衣服男女动漫周边学生短袖T恤夏可订制.', '2021-06-03', '中国', '无', '衣服', '无', '1', '0', '199', '266', '9', '1', '0');
INSERT INTO `tb_commo` VALUES ('48', '动漫二次元周边幸运盲盒好物16款值保底福袋礼盒礼物 初音', 'pics/动漫二次元周边幸运盲盒好物16款值保底福袋礼盒礼物 初音.jpg', '动漫二次元周边幸运盲盒好物16款值保底福袋礼盒礼物 初音.jpg', '2021-06-03', '中国', 'MARLIN', '动漫周边', '动漫', '100', '21', '66', '59.4', '9', '1', '0');
INSERT INTO `tb_commo` VALUES ('49', 'bilibili哔哩哔哩2233娘双面樱花折扇 印花扇子 B站周边 2233娘 双面樱花折扇', 'pics/bilibili哔哩哔哩2233娘双面樱花折扇 印花扇子 B站周边 2233娘 双面樱花折扇.jpg', 'bilibili哔哩哔哩2233娘双面樱花折扇 印花扇子 B站周边 2233娘 双面樱花折扇.jpg', '2021-06-03', '中国', '无', '扇子', '无', '99', '19', '69.9', '62.9', '9', '0', '1');

-- ----------------------------
-- Table structure for tb_form
-- ----------------------------
DROP TABLE IF EXISTS `tb_form`;
CREATE TABLE `tb_form` (
  `id` int(4) NOT NULL AUTO_INCREMENT COMMENT '自动编号',
  `formid` varchar(125) COLLATE utf8_unicode_ci NOT NULL COMMENT '订单号',
  `commo_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '商品id',
  `commo_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '商品名称',
  `commo_pics` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pics/null.jpg' COMMENT '商品图片',
  `commo_num` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '商品数量',
  `agoprice` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '商品单价',
  `fold` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '打折率',
  `total` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '消费总额',
  `vendee` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '消费者',
  `taker` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '收货人姓名',
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT '送货地址',
  `tel` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '联系电话',
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT '邮编',
  `pay_method` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '付款方式',
  `del_method` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '送货方式',
  `formtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '订单日期',
  `state` int(1) NOT NULL COMMENT '订单状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_form
-- ----------------------------
INSERT INTO `tb_form` VALUES ('1', '1298613711', '20,21', '液晶显示器,家庭影院', 'pics/null.jpg', '1,1', '5299.2,4399.2', '9,9', '9698.4', 'mr', 'sdf', 'wewewewe', 'wwewe', '130025', '银行转账', '平邮', '2011-02-25 14:01:51', '0');
INSERT INTO `tb_form` VALUES ('2', '1624793689', '43', '正版哔哩哔哩bilibili周边2020新版小电视泡面碗套装二次元 小电视泡面碗 现货', 'pics/正版哔哩哔哩bilibili周边2020新版小电视泡面碗套装二次元 小电视泡面碗 现货.jpg', '5', '180', '9', '900', 'Belong', '去去去', '中国', '15916517595', '514500', '银行转账', '平邮', '2021-06-27 11:34:49', '0');
INSERT INTO `tb_form` VALUES ('3', '1624803844', '48,21', '动漫二次元周边幸运盲盒好物16款值保底福袋礼盒礼物 初音,overlord 雅儿贝德手办骨王不死者', 'pics/动漫二次元周边幸运盲盒好物16款值保底福袋礼盒礼物 初音.jpg,pics/overlord 雅儿贝德手办骨王不死者.jpg', '4,6', '59.4,79.2', '9,9', '712.8', '南苑股东', '去去去', '中国', '15916517595', '514500', '银行转账', '平邮', '2021-06-27 14:24:04', '0');
INSERT INTO `tb_form` VALUES ('4', '1624845921', '41', '我的青春恋爱物语果然有问题 雪之下雪乃', 'pics/我的青春恋爱物语果然有问题 雪之下雪乃.jpg', '4', '269.1', '9', '1076.4', '张三', '去去去', '中国', '15916517595', '514500', '银行转账', '快递', '2021-06-28 02:05:21', '0');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(4) NOT NULL AUTO_INCREMENT COMMENT '自动编号',
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT '用户名称',
  `password` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT '用户密码',
  `question` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT '密码保护',
  `answer` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT '问题答案',
  `realname` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT '真实姓名',
  `card` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT '身份证号',
  `tel` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT '手机',
  `phone` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT '座机',
  `Email` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Email',
  `QQ` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT 'QQ',
  `code` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT '邮编',
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT '地址',
  `addtime` datetime NOT NULL COMMENT '注册日期',
  `isfreeze` int(1) NOT NULL DEFAULT '0' COMMENT '是否冻结',
  `shopping` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '购物车信息',
  `consume` float NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'jing', '123456', '123456', '123456', '静', '220322821103653', '13604338784', '0431-12345678', 'pp@sina.com', '334', '130025', '长春市', '2010-11-16 01:36:53', '1', '', '4399.2');
INSERT INTO `tb_user` VALUES ('2', 'tang', '123456', '123456', '123456', '潘攀', '111111111111111111', '22222222222', '0431-1111111', 'pp@sina.com', '22222222', '130025', '北京市', '2010-12-06 05:14:10', '0', '43,1@49,1@40,1@48,1', '21354.3');
INSERT INTO `tb_user` VALUES ('3', 'Belong', '123456', '1234', '', 'quan zhang', '', '15916517595', '', '1965096859@qq.com', '1965096859', '', '中国', '2021-06-27 11:33:50', '0', '43,5', '900');
INSERT INTO `tb_user` VALUES ('4', '南苑股东', '123456', '1234', '', 'quan zhang', '', '15916517595', '', '1965096859@qq.com', '1965096859', '', '中国', '2021-06-27 14:22:32', '0', '48,4@21,6', '712.8');
INSERT INTO `tb_user` VALUES ('5', '张三', '123456', '1234', '', 'quan zhang', '', '15916517595', '', '1965096859@qq.com', '1965096859', '', '中国', '2021-06-28 02:03:56', '0', '41,4', '1076.4');
